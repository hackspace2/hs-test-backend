const app = require('express')()
const bodyParser = require('body-parser')
const cors = require('cors')

const Persona = require('./models/Persona.js')

app.use(cors())
app.use(bodyParser.json())

app.get('/',(req,res)=>{
    return res.json({message:'ok asd'})
})

app.post('/persona',(req,res)=>{
    console.log(req.body)
    Persona.create({
        nombre:req.body.nombre,
        apellido:req.body.apellido,
        edad:req.body.edad
    }).then(()=>{
        return res.json({message:'Se creo persona.'})
    })
})

app.get('/persona',(req,res)=>{
    Persona.find().then(data=>{
        return res.json({data:data})
    })
})

app.delete('/persona/:id',(req,res)=>{
    console.log(req.params.id)
    Persona.deleteOne({ _id: req.params.id }).then(()=>{
        return res.json({message:'Se borró a '+req.params.id})
    });
})

app.listen(4000,()=>{
    console.log('Escuchando en el puerto 4000...')
})