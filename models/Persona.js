const mongoose = require('mongoose');
mongoose.connect(
  'mongodb://localhost:27017/hs-test', 
  {useNewUrlParser: true, useUnifiedTopology: true}
);

var personaSchema = new mongoose.Schema({
  nombre:  String,
  apellido: String,
  edad:   String
});

module.exports =  mongoose.model('Persona', personaSchema);
